import { Component, OnInit } from '@angular/core';
import { OAuthService, OAuthEvent } from 'angular-oauth2-oidc';
import { Router, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  constructor(private oauthService: OAuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.oauthService.events.pipe(filter((event: OAuthEvent) => event.type === 'session_terminated'))
      .subscribe(() => {
        this.oauthService.logOut(true);
        console.log('User is logged out');
      });
  }

  public login() {
    this.oauthService.initImplicitFlow();
  }

  public logout() {
    this.oauthService.logOut();
  }

  public getClaims() {
    return this.oauthService.getIdentityClaims();
  }

  public goToTokenInfo() {
    this.router.navigate(['token-info'], {relativeTo: this.route});
  }

  public goToProducts() {
    this.router.navigate(['product'], {relativeTo: this.route});
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit {

  public productList: Array<string>;
  constructor(private http: HttpClient, private oauthService: OAuthService) { }

  ngOnInit() {
    this.getProducts();
  }

  public getProducts() {
    return this.http.get('http://localhost:8900/products')
      .subscribe((data: Array<string>) => this.productList = data);
  }

}

import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-token-info',
  templateUrl: './token-info.component.html',
  styleUrls: ['./token-info.component.sass']
})
export class TokenInfoComponent implements OnInit {

  constructor(private oauthService: OAuthService) { }

  ngOnInit() {
  }

  public getClaims() {
    return this.oauthService.getIdentityClaims();
  }

  public getClaimValuesAsArray() {
    return Object.values(this.getClaims());
  }

  public getClaimEntriesAsArray(): Array<string> {
    return Object.keys(this.getClaims());
  }

  public getDataSource(): Array<{key: string, value: string}> {
    const datasource = this.getClaimEntriesAsArray()
      .map((data, index) => {
        return {key: data, value: this.getClaimValuesAsArray()[index]};
      });
    return datasource;
  }

}

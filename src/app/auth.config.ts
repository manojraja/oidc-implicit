import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
    // issuer: 'http://am-eval-myproject.192.168.99.108.nip.io:80/openam/oauth2',
    issuer: 'http://localhost:8080/openam/oauth2',
    clientId: 'openam',
    responseType: 'token id_token',
    redirectUri: 'http://localhost:4200/index.html',
    scope: 'openid profile email',
    requireHttps: false,
    sessionChecksEnabled: true
};

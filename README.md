# OidcImplicit

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


CORS Filter
------------

    <filter>
        <filter-name>CORSFilter</filter-name>
        <filter-class>org.forgerock.openam.cors.CORSFilter</filter-class>
        <init-param>
            <description>
                Accepted Methods (Required):
                A comma separated list of HTTP methods for which to accept CORS requests.
            </description>
            <param-name>methods</param-name>
            <param-value>POST,GET,PUT,DELETE,PATCH,OPTIONS</param-value>
        </init-param>
        <init-param>
            <description>
                Accepted Origins (Required):
                A comma separated list of origins from which to accept CORS requests.
            </description>
            <param-name>origins</param-name>
            <param-value>http://localhost:4200, http://localhost:8080</param-value>
        </init-param>
        <init-param>
            <description>
                Allow Credentials (Optional):
                Whether to include the Vary (Origin)
                and Access-Control-Allow-Credentials headers in the response.
                Default: false
            </description>
            <param-name>allowCredentials</param-name>
            <param-value>true</param-value>
        </init-param>
        <init-param>
            <description>
                Allowed Headers (Optional):
                A comma separated list of HTTP headers
                which can be included in the requests.
            </description>
            <param-name>headers</param-name>
            <param-value>
              Authorization,Content-Type,iPlanetDirectoryPro,X-OpenAM-Username,X-OpenAM-Password,Accept,Accept-Encoding,Connection,Content-Length,Host,Origin,User-Agent,Accept-Language,Referer,Dnt,Accept-Api-Version,If-None-Match, X-Password ,X-Username, X-NoSession
            </param-value>
        </init-param>
        <init-param>
            <description>
                Exposed Headers (Optional):
                The comma separated list of headers
                which the user-agent can expose to its CORS client.
            </description>
            <param-name>exposeHeaders</param-name>
            <param-value>Access-Control-Allow-Origin,Access-Control-Allow-Credentials,Set-Cookie,WWW-Authenticate</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>CORSFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>